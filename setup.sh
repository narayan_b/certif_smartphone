#!/bin/bash

# Function to read MySQL details
read_mysql_details() {
    read -p "Enter MySQL host: " mysql_host
    read -p "Enter MySQL user: " mysql_user
    read -sp "Enter MySQL password: " mysql_password
    echo
    read -p "Enter MySQL database: " mysql_database
}

# Function to install requirements
install_requirements() {
    echo "Creating virtual environment..."
    python3 -m venv venv

    echo "Activating virtual environment..."
    source venv/bin/activate

    echo "Installing required packages..."
    pip install -r requirements.txt
}

# Function to replace MySQL details in the script files
replace_mysql_details() {
    files=("script/sale.py" "api/sale_api.py")
    for file in "${files[@]}"; do
        sed -i "s/host=\"localhost\"/host=\"$mysql_host\"/" $file
        sed -i "s/user=\"narayan23\"/user=\"$mysql_user\"/" $file
        sed -i "s/password=\"coucou123\"/password=\"$mysql_password\"/" $file
        sed -i "s/database=\"smartphone\"/database=\"$mysql_database\"/" $file
    done
}

# Function to set up the database
setup_database() {
    echo "Setting up the database..."
    mysql -h $mysql_host -u $mysql_user -p$mysql_password < database/sale.sql
}

# Main script execution
main() {
    # Read MySQL connection details from the user
    read_mysql_details

    # Install Python requirements
    install_requirements

    # Replace MySQL details in Python scripts
    replace_mysql_details

    # Set up the database
    setup_database

    echo "Setup completed. You can now run your scripts."
}

# Run the main function
main
