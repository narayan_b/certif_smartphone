DROP DATABASE IF EXISTS smartphone;
CREATE DATABASE IF NOT EXISTS smartphone;
USE smartphone;

CREATE TABLE marque (
  id INT PRIMARY KEY AUTO_INCREMENT,
  brand VARCHAR(255)
);

CREATE TABLE feature (
  id INT PRIMARY KEY AUTO_INCREMENT,
  type ENUM('Colors', 'Memory', 'Storage', 'Camera', 'Rating'),
  value VARCHAR(255)
);

CREATE TABLE telephone (
  id INT PRIMARY KEY AUTO_INCREMENT,
  model VARCHAR(255),
  marque_id INT,
  color_id INT,
  memory_id INT,
  storage_id INT,
  camera_id INT,
  rating_id INT,
  FOREIGN KEY (marque_id) REFERENCES marque(id),
  FOREIGN KEY (color_id) REFERENCES feature(id),
  FOREIGN KEY (memory_id) REFERENCES feature(id),
  FOREIGN KEY (storage_id) REFERENCES feature(id),
  FOREIGN KEY (camera_id) REFERENCES feature(id),
  FOREIGN KEY (rating_id) REFERENCES feature(id)
);

CREATE TABLE cost (
  id INT PRIMARY KEY AUTO_INCREMENT,
  original_price DECIMAL(10,2),
  selling_price DECIMAL(10,2),
  discount_rate DECIMAL(5,2),
  discount_price DECIMAL(10,2),
  telephone_id INT,
  FOREIGN KEY (telephone_id) REFERENCES telephone(id)
);


GRANT ALL PRIVILEGES ON smartphone.* TO 'narayan23'@'localhost';
