import pandas as pd
import re
import mysql.connector

file_path = './dataset/Sales.csv'
df = pd.read_csv(file_path)

# Convert indian rupees into euro and create their new column
conversion_rate = 0.011
df['Selling Price (Euro)'] = df['Selling Price'] * conversion_rate
df['Original Price (Euro)'] = df['Original Price'] * conversion_rate
df['Discount Price (Euro)'] = df['Discount'] * conversion_rate

# remove unnecessary columns
columns_to_drop = ['Selling Price', 'Original Price', 'Discount']
df = df.drop(columns=columns_to_drop)

# keep only 2 digit after point
columns_to_round = ['Selling Price (Euro)', 'Original Price (Euro)', 'Discount Price (Euro)', 'discount percentage']
df[columns_to_round] = df[columns_to_round].round(2)

def convert_storage(storage):
    # Match numbers with optional space and unit, including "Expandable Upto"
    match = re.match(r'(Expandable Upto\s+)?(\d+)\s*(GB|MB|TB|gb|mb|tb)?', str(storage), re.IGNORECASE)
    if match:
        expandable, size, unit = match.groups()
        size = int(size)
        if unit:
            unit = unit.upper()
            if unit == 'TB':
                size *= 1024  # Convert TB to GB
            elif unit == 'MB':
                size /= 1024  # Convert MB to GB
        # If no unit, assume it's already in GB
        return int(size)
    return None

# Apply the conversion to the Memory column
df['Memory'] = df['Memory'].apply(convert_storage)


# Apply the conversion to the Storage column
df['Storage'] = df['Storage'].apply(convert_storage)

# Remove space before and after the value
df[df.select_dtypes(include=['object']).columns] = df.select_dtypes(include=['object']).apply(lambda x: x.str.strip())

# Replace NaN values with empty string for string columns and with None for numerical columns
df = df.fillna('')  
df = df.where(pd.notnull(df), None)


# connection
connection = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="###",
    auth_plugin='mysql_native_password',
    database="smartphone"
)


cursor = connection.cursor()


def insert_feature(row, df, cursor, connection):
    def insert_and_get_id(type, value):
        cursor.execute("SELECT id FROM feature WHERE value = %s AND type = %s", (value, type))
        existing_entry = cursor.fetchone()
        if not existing_entry:
            cursor.execute("INSERT IGNORE INTO feature (type, value) VALUES (%s, %s)", (type, value))
            connection.commit()
            cursor.execute("SELECT id FROM feature WHERE value = %s AND type = %s", (value, type))
            existing_entry = cursor.fetchone()
        return existing_entry[0] if existing_entry else None

    color_id = insert_and_get_id("Colors", row["Colors"]) if 'Colors' in df.columns and row["Colors"] else None
    memory_id = insert_and_get_id("Memory", row["Memory"]) if 'Memory' in df.columns and row["Memory"] else None
    storage_id = insert_and_get_id("Storage", row["Storage"]) if 'Storage' in df.columns and row["Storage"] else None
    camera_id = insert_and_get_id("Camera", row["Camera"]) if 'Camera' in df.columns and row["Camera"] else None
    rating_id = insert_and_get_id("Rating", row["Rating"]) if 'Rating' in df.columns and row["Rating"] else None

    return color_id, memory_id, storage_id, camera_id, rating_id

# Insert data into tables
for index, row in df.iterrows():
    if 'Brands' in df.columns and row['Brands']:
        brand_name = row['Brands']
        cursor.execute("SELECT id FROM marque WHERE brand = %s", (brand_name,))
        existing_brand = cursor.fetchone()
        if not existing_brand:
            cursor.execute("INSERT INTO marque (brand) VALUES (%s)", (brand_name,))
            connection.commit()
            brand_id = cursor.lastrowid
        else:
            brand_id = existing_brand[0]

    color_id, memory_id, storage_id, camera_id, rating_id = insert_feature(row, df, cursor, connection)

    if 'Models' in df.columns and row['Models']:
        model = row["Models"]
        # Insert into the 'telephone' table
        cursor.execute("""
            INSERT INTO telephone 
            (model, marque_id, color_id, memory_id, storage_id, camera_id, rating_id) 
            VALUES (%s, %s, %s, %s, %s, %s, %s)
            """, (model, brand_id, color_id, memory_id, storage_id, camera_id, rating_id))
        connection.commit()
        telephone_id = cursor.lastrowid

        if 'Original Price (Euro)' in df.columns and 'Selling Price (Euro)' in df.columns and 'discount percentage' in df.columns and 'Discount Price (Euro)' in df.columns:
            original_price = row["Original Price (Euro)"]
            selling_price = row["Selling Price (Euro)"]
            discount_rate = row["discount percentage"]
            discount_price = row["Discount Price (Euro)"]

            cursor.execute("""
                INSERT INTO cost 
                (original_price, selling_price, discount_rate, discount_price, telephone_id) 
                VALUES (%s, %s, %s, %s, %s)
                """, (original_price, selling_price, discount_rate, discount_price, telephone_id))

            connection.commit()
