# 📊 Projet de Smartphones

Ce projet consiste en un ensemble de scripts Python pour traiter les données de smartphones, les stocker dans une base de données MySQL, fournir une API avec FastAPI et visualiser les données avec Dash.

## 📁 Structure du Projet

projet-vente-smartphones/
├── api/
│   └── sale_api.py
├── database/
│   └── sale.sql
├── front/
│   ├── sale_front.py
│   ├── rating_page.py
│   ├── model_brand.py
│   └── output_layout.py
├── script/
│   └── sale.py
├── requirements.txt
└── setup.sh


## 🛠️ Prérequis

- Python 3
- MySQL

## 🚀 Installation et Configuration

1. **Clonez le dépôt :**

    ```bash
    git clone https://gitlab.com/narayan_b/certif_smartphone.git
    cd projet-vente-smartphones
    ```

2. **Créer un fichier `requirements.txt` :**

    Listez toutes les bibliothèques Python nécessaires à votre projet. Par exemple :

    ```txt
    pandas
    mysql-connector-python
    fastapi
    uvicorn
    dash
    plotly
    requests
    ```

3. **Rendre le script exécutable :**

    ```bash
    chmod +x setup.sh
    ```

4. **Exécuter le script de configuration :**

    ```bash
    ./setup.sh
    ```

    Le script `setup.sh` :

    - Crée et active un environnement virtuel.
    - Installe les packages nécessaires.
    - Demande les détails de connexion MySQL et les intègre dans les scripts Python.
    - Configure la base de données MySQL.

## 📝 Utilisation

Après avoir exécuté le script de configuration, vous pouvez lancer les différents composants de votre projet :

- **Traitement et insertion des données :**

    ```bash
    source venv/bin/activate
    python script/sale.py
    ```

- **Démarrer l'API FastAPI :**

    ```bash
    source venv/bin/activate
    python api/sale_api.py
    ```

- **Démarrer l'interface Dash :**

    Pour chaque fichier Dash dans le dossier `front`, par exemple :

    ```bash
    source venv/bin/activate
    python front/sale_front.py
    ```

## 🎨 Fonctionnalités

- **Script de traitement (`script/sale.py`) :**
    - Lit les données à partir d'un fichier CSV.
    - Nettoie et transforme les données.
    - Insère les données nettoyées dans la base de données MySQL.

- **API (`api/sale_api.py`) :**
    - Fournit des points de terminaison pour accéder aux données de la base de données.
    - Permet de filtrer les données par marque, modèle, RAM, stockage, prix minimum et maximum.

- **Interface Dash (`front/*.py`) :**
    - Affiche des tableaux et des graphiques interactifs des données de smartphone.
    - Permet de filtrer et de visualiser les données de différentes manières.

