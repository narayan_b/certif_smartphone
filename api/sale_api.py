import mysql.connector
from fastapi import FastAPI
from typing import List, Optional

app = FastAPI()

# Database connection setup
connection = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database="smartphone"
)

cursor = connection.cursor(buffered=True)

@app.get("/")
def get_smartphone_filter():
    """
    Fetch all smartphone data from the database.

    Returns:
        List[dict]: A list of dictionaries containing smartphone details.
    """
    query = """
        SELECT 
            telephone.id AS telephone_id,
            marque.brand AS brand,
            telephone.model AS model,
            memory.value AS ram,
            storage.value AS storage,
            color.value AS color,
            camera.value AS camera,
            rating.value AS rating,
            cost.selling_price AS price,
            cost.original_price,
            cost.discount_rate,
            cost.discount_price
        FROM 
            telephone
        JOIN 
            marque ON telephone.marque_id = marque.id
        JOIN 
            feature AS memory ON telephone.memory_id = memory.id
        JOIN 
            feature AS storage ON telephone.storage_id = storage.id
        JOIN 
            feature AS color ON telephone.color_id = color.id
        JOIN 
            feature AS camera ON telephone.camera_id = camera.id
        JOIN 
            feature AS rating ON telephone.rating_id = rating.id
        JOIN 
            cost ON cost.telephone_id = telephone.id
    """
    cursor.execute(query)
    results = cursor.fetchall()
        
    # Get the column names dynamically from cursor.description
    column_names = [col[0] for col in cursor.description]

    response = [
        {
            column_names[i]: result[i]
            for i in range(len(result))
        }
        for result in results
    ]

    return response

@app.post("/")
def smartphone_filter(
    brand: Optional[List[str]] = None,
    model: Optional[List[str]] = None,
    ram: Optional[List[str]] = None,
    storage: Optional[List[str]] = None,
    min_price: Optional[List[float]] = None,
    max_price: Optional[List[float]] = None
):
    """
    Filter smartphones based on given criteria.

    Args:
        brand (Optional[List[str]]): List of brands to filter by.
        model (Optional[List[str]]): List of models to filter by.
        ram (Optional[List[str]]): List of RAM sizes to filter by.
        storage (Optional[List[str]]): List of storage sizes to filter by.
        min_price (Optional[float]): Minimum selling price to filter by.
        max_price (Optional[float]): Maximum selling price to filter by.

    Returns:
        List[dict]: A list of dictionaries containing filtered smartphone details.
    """
    if min_price : min_price = min_price[0]
    if max_price : max_price = max_price[0]

    query = """
        SELECT 
            telephone.id AS telephone_id,
            marque.brand AS brand,
            telephone.model AS model,
            memory.value AS ram,
            storage.value AS storage,
            color.value AS color,
            camera.value AS camera,
            rating.value AS rating,
            cost.selling_price AS price,
            cost.original_price,
            cost.discount_rate,
            cost.discount_price
        FROM 
            telephone
        JOIN 
            marque ON telephone.marque_id = marque.id
        JOIN 
            feature AS memory ON telephone.memory_id = memory.id
        JOIN 
            feature AS storage ON telephone.storage_id = storage.id
        JOIN 
            feature AS color ON telephone.color_id = color.id
        JOIN 
            feature AS camera ON telephone.camera_id = camera.id
        JOIN 
            feature AS rating ON telephone.rating_id = rating.id
        JOIN 
            cost ON cost.telephone_id = telephone.id
    """

    where_conditions = []
    parameters = []

    if brand:
        placeholders = ', '.join(['%s'] * len(brand))
        where_conditions.append(f"marque.brand IN ({placeholders})")
        parameters.extend(brand)

    if model:
        placeholders = ', '.join(['%s'] * len(model))
        where_conditions.append(f"telephone.model IN ({placeholders})")
        parameters.extend(model)

    if ram:
        placeholders = ', '.join(['%s'] * len(ram))
        where_conditions.append(f"memory.value IN ({placeholders})")
        parameters.extend(ram)

    if storage:
        placeholders = ', '.join(['%s'] * len(storage))
        where_conditions.append(f"storage.value IN ({placeholders})")
        parameters.extend(storage)

    if min_price is not None:
        where_conditions.append("cost.selling_price >= %s")
        parameters.append(min_price)

    if max_price is not None:
        where_conditions.append("cost.selling_price <= %s")
        parameters.append(max_price)

    if where_conditions:
        query += " WHERE " + (" AND ".join(where_conditions))

    cursor.execute(query, parameters)

    results = cursor.fetchall()

    # Get the column names dynamically from cursor.description
    column_names = [col[0] for col in cursor.description]

    response = [
        {
            column_names[i]: result[i]
            for i in range(len(result))
        }
        for result in results
    ]

    return response


@app.get("/ratings")
def get_smartphone_ratings():
    """
    Fetch smartphone ratings data from the database.

    Returns:
        List[dict]: A list of dictionaries containing smartphone ratings by brand and model.
    """
    query = """
        SELECT 
            marque.brand AS brand,
            telephone.model AS model,
            rating.value AS rating
        FROM 
            telephone
        JOIN 
            marque ON telephone.marque_id = marque.id
        JOIN 
            feature AS rating ON telephone.rating_id = rating.id
    """
    cursor.execute(query)
    ratings_data = cursor.fetchall()
    
    response = [
        {
            'brand': row[0],
            'model': row[1],
            'rating': row[2]
        }
        for row in ratings_data
    ]

    return response


# Example usage of the filter function
if __name__ == '__main__':
    # filtered_smartphones = smartphone_filter(
    #     brand=["SAMSUNG", "OPPO"],
    #     model=["GALAXY M31S"],
    #     ram=["8.0"],
    #     storage=["128.0", "32.0"],
    #     min_price=500,
    #     max_price=600
    # )

    # # Display the filtered results
    # print(filtered_smartphones)


# print(get_smartphone_filter())

    import uvicorn
    uvicorn.run(app, host="localhost", port=8000)