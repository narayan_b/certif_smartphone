import dash
from dash import dcc, html
import plotly.express as px
from dash.dependencies import Input, Output
import requests

# Initialize Dash app
app = dash.Dash(__name__)

# Define Dash layout
app.layout = html.Div([
    html.H1("Smartphone Models Distribution by Brand"),
    dcc.Graph(id='pie-chart'),
    dcc.Interval(
        id='interval-component',
        interval=60*1000,
        n_intervals=0
    )
])

@app.callback(
    Output('pie-chart', 'figure'),
    [Input('interval-component', 'n_intervals')]
)
def update_pie_chart(n):
    # Fetch smartphone data from the FastAPI route
    response = requests.get("http://127.0.0.1:8000/")
    smartphone_data = response.json()
    
    # Extract brand and model from the data and filter out duplicates
    unique_smartphones = {item['model']: item for item in smartphone_data}
    unique_smartphones = list(unique_smartphones.values())
    
    # Calculate the count of models for each brand
    brand_model_count = {}
    for item in unique_smartphones:
        brand = item['brand']
        if brand in brand_model_count:
            brand_model_count[brand] += 1
        else:
            brand_model_count[brand] = 1
    
    # Prepare data for the pie chart
    brands = list(brand_model_count.keys())
    model_counts = list(brand_model_count.values())
    
    # Create the pie chart
    fig = px.pie(
        names=brands,
        values=model_counts,
        title="Smartphone Models Distribution by Brand",
        labels={'names': 'Brand', 'values': 'Total models'},
        hover_data={'names': brands, 'values': model_counts}
    )

    # Update hover template to show custom labels
    fig.update_traces(hovertemplate='<b>Brand:</b> %{label}<br><b>Total models:</b> %{value}')

    return fig

if __name__ == '__main__':
    app.run_server(debug=True)
