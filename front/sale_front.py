import dash
from dash import dcc, html, Input, Output, dash_table
import pandas as pd
import requests

# Initialize Dash app
app = dash.Dash(__name__)

# Function to fetch data from the FastAPI endpoint
def fetch_data(filters):
    api_url = "http://127.0.0.1:8000/"
    try:
        response = requests.post(api_url, json=filters)
        response.raise_for_status()
        return response.json()
    except requests.RequestException as e:
        print(f"Error fetching data from API: {e}")
        return []

def update_options(selected_data, column_name):
    if column_name in selected_data.columns:
        return [{'label': value, 'value': value} for value in selected_data[column_name].unique()]
    else:
        return []

# Initialize layout
app.layout = html.Div([
    html.H1("Find your best smartphone"),

    html.Div([
        html.Label('Brand'),
        dcc.Dropdown(
            id='brand-dropdown',
            multi=True,
            style={'width': '100%', 'height': '40px'}
        ),
    ], style={'width': '48%', 'display': 'inline-block'}),

    html.Div([
        html.Label('Model'),
        dcc.Dropdown(
            id='model-dropdown',
            multi=True,
            style={'width': '100%', 'height': '40px'}
        ),
    ], style={'width': '48%', 'display': 'inline-block', 'marginLeft': '4%'}),

    html.Div([
        html.Label('Memory'),
        dcc.Dropdown(
            id='memory-dropdown',
            multi=True,
            style={'width': '100%', 'height': '40px'}
        ),
    ], style={'width': '48%', 'display': 'inline-block'}),

    html.Div([
        html.Label('Storage'),
        dcc.Dropdown(
            id='storage-dropdown',
            multi=True,
            style={'width': '100%', 'height': '40px'}
        ),
    ], style={'width': '48%', 'display': 'inline-block', 'marginLeft': '4%'}),

    html.Div([
        html.Label('Min Price'),
        dcc.Input(
            id='min-price-input',
            type='number',
            placeholder='Min price',
            style={'width': '100%', 'height': '40px'}
        ),
    ], style={'width': '48%', 'display': 'inline-block'}),

    html.Div([
        html.Label('Max Price'),
        dcc.Input(
            id='max-price-input',
            type='number',
            placeholder='Max price',
            style={'width': '100%', 'height': '40px'}
        ),
    ], style={'width': '48%', 'display': 'inline-block', 'marginLeft': '4%'}),

    html.Div(id='result-count', style={'marginTop': '20px', 'marginBottom': '20px', 'fontWeight': 'bold', 'fontSize': '20px'}),

    dash_table.DataTable(
        id='table',
        page_size=10,
        style_table={'overflowX': 'auto'},
        style_header={
            'backgroundColor': '#223359',
            'color': 'white',
            'fontWeight': 'bold'
        },
        style_data={
            'backgroundColor': '#ABCFFF',
            'color': 'black'
        }
    ),
])

@app.callback(
    [Output('brand-dropdown', 'options'),
     Output('model-dropdown', 'options'),
     Output('memory-dropdown', 'options'),
     Output('storage-dropdown', 'options'),
     Output('table', 'data'),
     Output('result-count', 'children')],
    [Input('brand-dropdown', 'value'),
     Input('model-dropdown', 'value'),
     Input('memory-dropdown', 'value'),
     Input('storage-dropdown', 'value'),
     Input('min-price-input', 'value'),
     Input('max-price-input', 'value')]
)
def update_options_and_table(selected_brands, selected_models, selected_memory, selected_storage, min_price, max_price):
    if not min_price : min_price = 0.0
    if not max_price : max_price = 9999999999.99
    filters = {
        "brand": selected_brands,
        "model": selected_models,
        "ram": selected_memory,
        "storage": selected_storage,
        "min_price": [min_price],
        "max_price": [max_price]
    }
    data = fetch_data(filters)
    df = pd.DataFrame(data)

    brand_options = update_options(df, 'brand')
    model_options = update_options(df, 'model')
    memory_options = update_options(df, 'ram')
    storage_options = update_options(df, 'storage')
    result_count_message = f"{len(df)} smartphones found."
    return brand_options, model_options, memory_options, storage_options,  df.to_dict('records'), result_count_message

if __name__ == '__main__':
    app.run_server(debug=True)