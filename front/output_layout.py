import dash
from dash import Dash, html, dcc, callback, Input, Output

# Initialize the Dash app without using a specific pages folder
app = Dash(__name__, use_pages=True, pages_folder="", suppress_callback_exceptions=True)

# Define the layout of the main page
app.layout = html.Div([
    html.H1('Multi-page app with Dash Pages'),
    html.Div([
        html.Button('Go to Sale Front', id='btn-sale-front', n_clicks=0),
        html.Button('Go to Rating Page', id='btn-rating-page', n_clicks=0),
        html.Button('Go to Model Brand', id='btn-model-brand', n_clicks=0),
    ]),
    # Placeholder div to hold the page content
    html.Div(id='page-container')
])

# Callback to handle button clicks and navigate to the respective pages
@callback(
    Output('page-container', 'children'),
    [Input('btn-sale-front', 'n_clicks'),
     Input('btn-rating-page', 'n_clicks'),
     Input('btn-model-brand', 'n_clicks')]
)
def display_page(btn1, btn2, btn3):
    ctx = dash.callback_context

    if not ctx.triggered:
        button_id = 'None'
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == 'btn-sale-front':
        return dcc.Location(href='/sale_front', id='location')
    elif button_id == 'btn-rating-page':
        return dcc.Location(href='/rating_page', id='location')
    elif button_id == 'btn-model-brand':
        return dcc.Location(href='/model_brand', id='location')
    else:
        return html.Div('Welcome to the multi-page app!')

if __name__ == '__main__':
    app.run_server(debug=True)
