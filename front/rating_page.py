from dash import Dash, dcc, html, Input, Output
import plotly.express as px
import requests

# Fetch ratings data from the FastAPI route
def fetch_ratings_data():
    response = requests.get('http://localhost:8000/ratings')
    if response.status_code == 200:
        return response.json()
    else:
        return []

# Fetch ratings data
ratings_data = fetch_ratings_data()

# Get unique brands for dropdown options
unique_brands = set(row['brand'] for row in ratings_data)

# Initialize Dash app
app = Dash(__name__, path='/rating_page')

# Define the layout of the Dash app
app.layout = html.Div([
    html.H1("Smartphone Ratings Dashboard"),
    dcc.Dropdown(
        id='brand-dropdown',
        options=[{'label': brand, 'value': brand} for brand in unique_brands],  # Provide the list of unique brands
        placeholder="Select a brand",
        multi=False
    ),
    dcc.Graph(id='rating-histogram')
])

# Define callback to update histogram based on brand selection
@app.callback(
    Output('rating-histogram', 'figure'),
    [Input('brand-dropdown', 'value')]
)
def update_histogram(selected_brand):
    if selected_brand:
        # Filter ratings data based on selected brand
        filtered_data = [row for row in ratings_data if row['brand'] == selected_brand]
        # Convert ratings to float
        for row in filtered_data:
            row['rating'] = float(row['rating'])
        # Calculate the average rating per model
        model_ratings = {}
        for row in filtered_data:
            if row['model'] not in model_ratings:
                model_ratings[row['model']] = []
            model_ratings[row['model']].append(row['rating'])
        
        model_avg_ratings = [{'model': model, 'rating': sum(ratings)/len(ratings), 'brand': selected_brand} for model, ratings in model_ratings.items()]
        
        fig = px.bar(
            model_avg_ratings,
            x='model',
            y='rating',
            title=f"Ratings for {selected_brand}",
            labels={'model': 'Model', 'rating': 'Rating'},
            hover_data={'brand': True, 'rating': ':.2f'}
        )
    else:
        # Calculate average rating for each brand
        brand_ratings = {}
        for row in ratings_data:
            if row['brand'] not in brand_ratings:
                brand_ratings[row['brand']] = []
            # Convert rating to float to ensure numeric operations
            brand_ratings[row['brand']].append(float(row['rating']))
        
        brand_avg_ratings = [{'brand': brand, 'rating': sum(ratings) / len(ratings)} for brand, ratings in brand_ratings.items()]

        fig = px.bar(
            brand_avg_ratings,
            x='brand',
            y='rating',
            title="Average Ratings by Brand",
            labels={'brand': 'Brand', 'rating': 'Average Rating'},
            hover_data={'brand': True, 'rating': ':.2f'}
        )

    return fig

# Run the Dash app
if __name__ == '__main__':
    app.run_server(debug=True)
